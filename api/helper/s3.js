require("dotenv").config();

const aws = require("aws-sdk");
const fs = require("fs");
const multer = require("multer");

const path = require("path");
const sharp = require("sharp");







const bucketName = process.env.AWS_BUCKET_NAME
    //   region = process.env.AWS_REGION,
    //   accessKeyId = process.env.AWS_ACCESS_KEY,
    //   secretAccessKey = process.env.AWS_SECRET_KEY;

const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    accessSecretKey: process.env.AWS_SECRET_KEY,
    region: process.env.AWS_REGION
})

//upload the image


exports.addimge = async function (req, res) {
    const { filename: image } = req.file
    req.file.filename = `user-${Date.now()}.jpeg`;
    console.log("req.file", req.file, "req.file.path ----", req.file.path)
    const imagecompressed = await sharp(req.file.path)
        .resize(200)
        .jpeg({ quality: 90, progressive: true, })
        .toFile(`/home/ts/Documents/demo/nodeapi/uploads/${req.file.filename}`).then(data => { console.log("data", data) })
        .catch(err => { console.log("err", err) });

    //fs.unlinkSync(req.file.path)
    console.log("image --", imagecompressed)
    return res.send('SUCCESS!')

}

// reduct image to 40%;
async function compressorImg(img){
//
    console.log(img)
    return sharp(img.path)
        .resize(400, 200 , {
            fit: "contain"
        })
        .jpeg({ quality: 40, progressive: true})
        .toFile(`/uploads/user-${Date.now()}.jpeg`)
        .then(data => { //how to return image itself? 
             console.log(data) 
        })
        
    
        // sharp(img.path)
        //     .toFormat('jpeg')
        //     .toFile(`uploads/new_file.jpeg`, (err, info)=> {
        //     if(err){
        //         console.log("eeeeeeeeerrrrrrrrrrrr", err    )
        //         return false
        //     }else{
        //         console.log("done", info)
        //     }
            
        // })  
}

async function mediumImg(img){
    await sharp(img.path)
            .resize(600,600, {
                fit:"contain"
            })
            .toFile(`${img.filename}.jpg`, (err, info) => {
                if(err){
                    return false
                }
            })
            .then( info => {
                return `${img.filename}.jpg`
            })
}

  
// main functin for uploading file
function uploadFile(file, type,size){ 
   
    const fileStream = fs.createReadStream(file.path)
    const uploadParams = {
        Bucket: bucketName,
        Body: fileStream,
        Key: `${type}/${size}/${file.filename}`
    }
    return s3.upload(uploadParams).promise()
}


async function uploadReduceFile(file){
    await sharp(file.path)
        .jpeg({quality: 40})  
        .then((err, outputBuffer)=> {
            if(err){
                return err
            }
            else{
                let data = outputBuffer.toString("base64");
                let newdata = Buffer.from(data, "base64");
                let imageee = fs.writeFileSync("new-path.jpg", newdata);
                console.log("new data", imageee ) 
                return imageee
            }
        })
        
    
}

async function imageType(image, type, size) {

    
    size.forEach(async siz => {
        switch(siz){
            // for original size
            // case 1: {
            //    //aa done che.. compress image thi karvanu che.. aema guide karjo ok
            //     await uploadFile(image, type, "originalSize")
            //     break
            // }
            //for reduce size
            case 2: {
                await compressorImg(image)
                break
            }
            // case 3: {
            //     await uploadFile(await mediumImg(image), type, "mediumSize")
            //     break
            // }
            // case 4: {
            //     await uploadFile(await smallImg(image), type,"smallSize")
            //     break
            // }
            default: return {message: "plz enter valid size type",success:false, undefined, status: 400 }
        }

    })

}

function smallImg(img){
    return 1
}

// upload multiple images
function uploadMulFileS3(images, type, size){
    
  
    console.log(images)

    images.forEach(async (image, index) => {
        switch(type){
            //for customer
            case '1': 
                await imageType(image, "customer", size)
                break
            
            //for vendor
            case '2': 
                console.log("--------vendor", size)
                console.log("vendor image", image)
                await imageType(image, "vendor", size)
                break
            

            //for product
            case '3': 
                await imageType(image, "product", size)
                break
            
            default: 
                return {message: "plz enter valid type of  product", success: false, undefined,status: 400}

            
        }
        if(images.length-1 == index){
            return {message: "image uploaded successfully", success:true, undefined, status: 200}
        }
      
    })
}





//download the image



module.exports = { uploadFile, uploadMulFileS3 , uploadReduceFile}
      