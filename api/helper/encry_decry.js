const CryptoJS = require("crypto-js")
require("dotenv").config();



function encrypt(text) {
    console.log(text)
    if (text) return CryptoJS.AES.encrypt(JSON.stringify(text), process.env.ENCRYPTION_KEY.trim()).toString();

    
}

function decrypt(text) {
    if (text) return JSON.parse(CryptoJS.AES.decrypt(text, process.env.ENCRYPTION_KEY.trim()).toString(CryptoJS.enc.Utf8));
}

module.exports = { encrypt, decrypt}