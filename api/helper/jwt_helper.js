const jwt = require("jsonwebtoken");
const { decrypt } = require("./encry_decry");

require("dotenv").config();


function verifyAccessToken (req, res, next) {
    console.log(req.headers)
    

    let  token = req.headers["authorization"]
   
    token = token.split(" ")[1]
    if(!token){
        res.status(403).json({success: false, message: "token missing"})
    }else{
        jwt.verify(token, process.env.ACCESS_TOKEN,(err, payload) => {
            if(err){
                res.status(403).json({success:false, message: "unauthrized token"})
            }else{
                
                req.body.user_id =  decrypt(payload.user_id)
                next();
            }
        })
    }
    
}


module.exports = { verifyAccessToken}



