const categoryModel = require("../../model/vendor_auth");

class vendorauthservices {
  async verify_vender_list_find(
    { mobileno },
    { aadhar_card },
    id,
    body,
    callback
  ) {
    let location = new categoryModel({
      mobileno: mobileno,
      aadhar_card: aadhar_card,
      vendor_status: "active",
    });
    categoryModel
      .findOneAndUpdate(id, body, { new: true })
      .then((res) => {
        console.log("eres", res);
        callback(null, res);
      })
      .catch((err) => {
        callback(null, err);
      });
  }

  async verify_status_update(id, body, callback) {
    locationModel
      .findOneAndUpdate(id, body, { new: true })
      .then((res) => {
        console.log("eres", res);
        callback(null, res);
      })
      .catch((err) => {
        callback(null, err);
      });
  }

  async verify_vendor_list_active(callback) {
    await categoryModel
      .find({ vendor_status: "active" })
      .then((res) => {
        callback(null, res);
      })
      .catch((err) => {
        callback(null, err);
      });
  }

}

module.exports = vendorauthservices;
