const locationModel = require("../../model/location");

class addlocationservices {
  async addlocation(pincode, area, callback) {
    let location = new locationModel({
      pincode: pincode,
      area: area,
      accessBy: "vendor",
    });
    await location
      .save()
      .then((res) => {
        console.log("resssaddd", res);
        callback(null, res);
      })
      .catch((err) => {
        console.log("errrlocation", err);
        callback(err, null);
      });
  }

  async getlocation(pincode, callback) {
    await locationModel
      .find({ pincode })
      .then((res) => {
        callback(null, res);
      })
      .catch((err) => {
        callback(null, err);
      });
  }

  async editlocation(id, body, callback) {
    locationModel
      .findOneAndUpdate(id, body, { new: true })
      .then((res) => {
        console.log("eres", res);
        callback(null, res);
      })
      .catch((err) => {
        callback(null, err);
      });
  }
  async location_list(_id, callback) {
    locationModel
      .findById(_id)
      .then((res) => {
        callback(null, res);
      })
      .catch((err) => {
        callback(null, err);
      });
  }
}

module.exports = addlocationservices;
