const categoryModel = require('../../model/category');


class categoryservices{

    async addcategory(category,sub_category,callback){
        let categorydata =  new categoryModel(
            {
                category: category,
                sub_category:sub_category
               
            })
            await categorydata.save().then(res => {
                callback(null, res)
            }).catch(err => {
                callback(err, null);
            })
    }

    async getallcategory(callback){
        await categoryModel.find()
        .then(res => { 
            callback(null, res);
        }).catch(err => {
            callback(null,err);
        })
    }

    async getcategorybyid(_id,callback){
        categoryModel.findById(_id)
        .then(res => { 
            callback(null, res);
        }).catch(err => {
            callback(null,err);
        })
    }
}
module.exports = categoryservices