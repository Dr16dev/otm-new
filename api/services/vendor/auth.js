const vendor_auth_modal = require ('../../model/vendor_auth');
const vendor_img = require ('../../model/vendor_img');
const otp_model = require ('../../model/otp');

class vender_auth_services {
  async save_vendor_register (registerdata, callback) {
    var verification = {
      aadhar_card_verified: 'pending',
      address_proof_verified: 'pending',
      bank_detail_verified: 'pending',
    };
    let data = vendor_auth_modal ({
      mobileno: registerdata.mobileno,
      emailid: registerdata.emailid,
      vendor_name: registerdata.vendor_name,
      password: registerdata.password,
      aadhar_card: registerdata.aadhar_card,
      aadhar_card_front_img: registerdata.aadhar_card_front_img,
      aadhar_card_back_img: registerdata.aadhar_card_back_img,
      cancel_check_img: registerdata.cancel_check_img,
      address_proof_img: registerdata.address_proof_img,
      address_proof_img: registerdata.shop_cover_img,
      address_proof_img: registerdata.vendor_profile_img,
      shop_address: registerdata.shop_address,
      bank_detail: registerdata.bank_detail,
      gstno: registerdata.gstno,
      shop_name: registerdata.shop_name,
      location: registerdata.location,
      vendor_status: 'pending',
      verification: verification,
      status_verified: false,
      is_email_verified: false,
      is_mobile_verified: false,
      is_deleted: false,
      follower_user: [],
      isKYC: false,
    });
    await data
      .save ()
      .then (res => {
        callback (null, res);
      })
      .catch (err => {
        callback (err, null);
      });
  }

  async save_register_image (data, callback) {
    let vendor_image = vendor_img ({
      vendor_id: data._id,
      aadhar_card_front_img,
      aadhar_card_back_img,
      cancel_check_img,
      address_proof_img,
    });
    await vendor_image
      .save ()
      .then (res => {
        callback (null, res);
      })
      .catch (err => {
        callback (err, null);
      });
  }

  async editvendor (id, req, callback) {
    await vendor_auth_modal
      .findOneAndUpdate (id, req)
      .then (res => {
        console.log ('res', res);
        callback (null, res);
      })
      .catch (err => {
        callback (null, err);
      });
  }

  async find_vender_pincode (callback) {
    await vendor_auth_modal
      .find ()
      .then (res => {
        console.log ('res', res);
        callback (null, res);
      })
      .catch (err => {
        callback (null, err);
      });
  }
  async send_otp_save (mobileno, otp, callback) {
    let send_otp = otp_model ({
      mobileno: mobileno,
      otp: otp,
    });
    await send_otp
      .save ()
      .then (res => {
        console.log ('res', res);
        callback (null, res);
      })
      .catch (err => {
        callback (null, err);
      });
  }

  async reset_password_update (mobileno, password, callback) {
    await vendor_auth_modal
      .updateOne (mobileno, password, {new: true})
      .then (res => {
        console.log ('res', res);
        callback (null, res);
      })
      .catch (err => {
        callback (null, err);
      });
  }

  // async set_rejected_vendor_update(id,vendor_edit,callback){
  //     console.log("id",id)
  //     await vendor_auth_modal.findByIdAndUpdate(id, vendor_edit)
  //     .then((res) => {
  //         console.log("res",res)
  //       callback(null, res);
  //     })
  //     .catch((err) => {
  //       callback(null, err);
  //     });
  // }
}

module.exports = vender_auth_services;
