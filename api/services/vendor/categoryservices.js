const category_model = require('../../model/category');

class vendor_category_services {
  async searchcategory(re, callback) {
    await category_model
      .find().or([{ 'category': { $regex: re }}, { 'sub_category': { $regex: re }}])
      .then((res) => {
          console.log("res",res)
        callback(null, res);
      })
      .catch((err) => {
        callback(null, err);
      });
  }
}

module.exports = vendor_category_services