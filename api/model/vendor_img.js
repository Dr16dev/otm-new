const { model, Schema } = require("mongoose")

let vendor_img = new Schema ({
    aadhar_card_front_img: {
        type:String
    },
    aadhar_card_back_img: {
        type:String
    },
    cancel_check_img: {
        type: String
    },
    address_proof_img:{
        type:String
    },
    vendor_id: {
        type:Schema.Types.ObjectId,
        ref:"vendor_auth"
    }
})
module.exports = model("vendor_img", vendor_img)