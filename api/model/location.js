const { Schema, model } = require("mongoose");


const locationSchema = new Schema({
    pincode:{
        type:Number,
        required:true,
        unique:true
    },
    area: [{
        type: String,
        unique:[true, "it's already exist try other location name"]
    }]
})

module.exports = model("location", locationSchema)