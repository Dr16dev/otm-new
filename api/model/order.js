const { model, Schema } = require("mongoose");

const orderSchema = new Schema({
    products:[{
        product_id: {
            type:Schema.Types.ObjectId,
            required:true,
        },
        sku: {
            type:String,
            required: true
        },
        quantity:
        {
            type: Number, 
            required: true
        },
        price:{
            type:Number,
            required:true
        }
    }],
    discount:{
        type: Number
    },
    total:{
        type:Number,
        required:true
    },
    grand_total:{
        type:Number,
        required:true
    },
    reseller_by:{
        type:Schema.Types.ObjectId,
        required:true
    },
    order_status:{
        type: String, 
        enum: ["Pending", "Completed", "Rejected"],
        default: "Pending"
    },
    payment:{
        payment_type: {
            type:String,
            enum: ["Cod", "Upi"],
            required:true
        }
    },
    order_tracking: [{
        updated_by: {
            updated_user_type:{
                type: String,
                enum:["vendor", "Reseller", "Collection_boy", "Delivery_boy"]
            },
            updated_user_id:{
                type: Schema.Types.ObjectId,
                required:true,
                refPath:"updated_type"
            },
            updated_type:{
                type:String,
                required:true,
                enum: ["vendor_auth", "Reseller", "Delivery_user"]
            }
            

        },
    
        status:  {
            type: String,
            enum: ["Received", "Completed", "Cancel"]
        },
        reason_for_cancel:{
            type: String
        },
        updated_at:{
            type: Date()
        }
    }]

})



module.exports = model("Order", orderSchema)