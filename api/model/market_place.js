const { model, Schema } = require("mongoose");

const market_place = new Schema({
    market_place: {
        type: String,
        required: true
    }
})

module.exports = model("market_place", market_place)