const { model, Schema} = require("mongoose")


const deliver_user_schema = new Schema({
    user_type:{
        type:String,
        enum:["delivery_boy", "colection_boy", "collection_admin"]
    },
    first_name: {
        type: String,
        minLength: [3, "atlease required 3 character as first name"],
        maxLenght:[255, "no more than 255 character allowd in name"],
        required: true
    },
    last_name: {
        type:String,
        minLength: [3, "atlease required 3 character as first name"],
        maxLenght:[255, "no more than 255 character allowd in name"],
        required: true
        
    },
    mobileno: {
        type:Number,
        required:[true, "for login and registartion , mobile number required "]
    },
    password:{
        type:String,
        required:[true, "password is necessory"]
    }

})

module.exports = model("Deliver_user", deliver_user_schema)