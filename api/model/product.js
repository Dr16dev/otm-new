const { Schema, model} = require("mongoose");

const product_image= new Schema({
    originalSize: {
        type: String
    },
    reduceSize: {
        type: String
    },
    mediumSize: {
        type: String
    },
    is_primary:{
        type: Boolean
    }

})
const product = new Schema({
    product_images:[
        { type: product_image } 
    ],
    product_title: {
        type: String
    },
    product_description: {
        type: String
    },
    product_price: {
        type: Number
    },
    product_type: {
        type: String
    },
    product_variation:[{
        _id: Schema.Types.ObjectId, 
        sku: {
            type:String,
            required:true
        },
        color: {
            type: String,
            required:true
        },
        size:{
            type: String,
            required:true
        },
        price: {
            type: String,
            required:true
        },
        variation_img: {
            type: String
        }
    }] ,
    vendor_id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'vendor'
    },
    category_id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'category'
    },
    qty: {
        type: Number
    },
    listing_status: {
        type: String,
        enum: ["active", "deactive", "delete", "sold_out", "stock_out"],
        default: "active"
    },
    is_public: {
        type: Boolean
    }
});


module.exports = model("Product", product)

