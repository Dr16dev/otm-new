const {TimestreamQuery} = require ('aws-sdk');
const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

let aadhar_card = new Schema ({
  aadhar_card_number: {
    type: Number,
    required: true,
  },
  DOB: {
    type: Date,
    required: true,
  },
  aadhar_card_name: {
    type: String,
    required: true,
  },
  aadhar_card_front_img: {
    type: String,
  },
  aadhar_card_back_img: {
    type: String,
  },
});

let bank_detail = new Schema ({
  bank_name: {
    type: String,
    required: true,
  },
  bank_ifsc_code: {
    type: String,
    required: true,
  },
  account_number: {
    type: String,
    required: true,
  },
  account_holder_name: {
    type: String,
    required: true,
  },
});

let vendor_auth = new Schema ({
  mobileno: {
    type: Number,
    required: true,
  },
  emailid: {
    type: String,
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      'Please fill a valid email address',
    ],
    required: true,
  },
  vendor_name: {
    type: String,
    match: [
      /^[a-zA-Z0-9]{4,45}$/,
      'please enter proper name no specical character allowed vendor name size must around 4 to 45',
    ],
  },
  vendor_description: {
    type: String,
  },
  password: {
    type: String,
  },
  shop_cover_img: {
    type: String,
  },
  vendor_profile_img: {
    type: String,
  },
  shop_address: {
    type: String,
  },
  shop_google: [],
  gstno: {
    type: String,
  },
  is_mobile_verified: {
    type: Boolean,
  },
  is_email_verified: {
    type: Boolean,
  },
  status_verified: {
    type: Boolean,
  },
  is_deleted: {
    type: Boolean,
  },
  follower_user: [],
  browser: [
    {
      ip_address: {
        type: String,
        required: true,
      },
      user_client: {
        type: String,
      },
    },
  ],
  shop_name: {
    type: String,
  },
  location: {
    state: {
      type: String,
    },
    city: {
      type: String,
    },
    pincode: {
      type: Number,
    },
    market_place: {
      type: String,
    },
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  pincode: {
    type: Number,
  },
  verification: {
    aadhar_card_verified: {
      type: String,
      enum: ['pending', 'accept', 'reject'],
      default: 'pending',
      required: true,
    },
    address_proof_verified: {
      type: String,
      enum: ['pending', 'accept', 'reject'],
      defualt: 'pending',
      required: true,
    },
    bank_detail_verified: {
      type: String,
      enum: ['pending', 'accept', 'reject'],
      default: 'pending',
      required: true,
    },
  },

  vendor_status: {
    type: String,
    enum: ['active', 'pending', 'inactive'],
    required: true,
  }, // active pending inactive

  aadhar_card: {
    type: aadhar_card,
  },
  rejected_list: [
    {
      rejected_type: {
        type: String,
        enum: ['aadhar', 'address', 'bank'],
      },
      rejected_reason: {
        type: String,
      },
    },
  ],
  bank_detail: {
    type: bank_detail,
  },
  vendor_img: {
    type: Schema.Types.ObjectId,
    ref: 'vendor_img',
  },
  market_place: {
    type: String,
  },
});

module.exports = mongoose.model ('vendor_auth', vendor_auth);
