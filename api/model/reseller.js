const { model, Schema } = require("mongoose");

const resellerSchema = new Schema({
    first_name:{
        type:String,
        minLength:[1, "at least have one character as first name"],
        maxLength:[255, "name is too big right something small name"],
        required:true
    },
    last_name:{
        type:String,
        minLength:[1, "at least have one character as first name"],
        maxLength:[255, "name is too big right something small name"],
        required:true
    },
    mobileno:{
        type: Number,
        required:true
    },
    following_vendor: [ { type: Schema.Types.ObjectId , ref:"vendor_auth" }],
    password: {
        type: String,
        required: true
    },
    emailid:{
        type:String
    },
    address: [{
        mobileno_for_address: {
            type: Number,
            required:true
        },
        address_name: {
            type: String
        },
        state:{
            type: String,
            required:[true, "state is required"]
        },
        city: {
            type: String,
            required:[true, "city is required"]

        },
        pincode: {
            type: String,
            required:[true, "pincode is necessary for deliver the prodcut"]
        },
        address_descrption:{
            type: String,
            min:[1, "atleast have something"]
        
        }
        
    }],
    add_to_cart:[{
        product_id: {
            type:Schema.Types.ObjectId,
            ref: "Product"
        },
        quantity: {
            type:Number
        }
    }]

    

})

module.exports = model("Reseller", resellerSchema)