const express = require("express")
const { search_category } = require("../../controller/vendor/category")
const { verifyAccessToken } = require("../../helper/jwt_helper")
const { activeVendor } = require("../../middleware/activeVendor")
let vendorAuth = require("../../controller/vendor/auth")
const route = express.Router()


route.get("/search_category",   search_category)

/**
 * @swagger
 * /category/search_category:
 *   get:
 *     tags:
 *       - vender
 *     description: search category and sub_category list
 *     produces:
 *       - application/json
 *     responses:
 *          200:
 *              description: Data fetched successfully
 *          400: 
 *              description: no data found with the containing {search data name} try something else
 *          404: 
 *              somthing went wrong please contact admin 
 */

module.exports = route
