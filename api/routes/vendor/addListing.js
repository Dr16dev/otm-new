const express = require("express")
const { vendor_add_product } = require("../../controller/vendor/listing")
const { verifyAccessToken } = require("../../helper/jwt_helper")
const { activeVendor } = require("../../middleware/activeVendor")

const route = express.Router()


route.post("/add_listing", verifyAccessToken, activeVendor, vendor_add_product)



module.exports = route
