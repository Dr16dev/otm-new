let route = require ('express').Router ();
const uploadImg = require ('../../middleware/imgUpload');
const fs = require ('fs');
let vendorAuth = require ('../../controller/vendor/auth');
const {verifyAccessToken} = require ('../../helper/jwt_helper');
const sharp = require ('sharp');
const {vendorImageUpload} = require ('../../middleware/vendorImageUpload');
const multer = require ('multer');

//swagger

/**
 * @swagger
 * /vendor/vendor_register:
 *   post:
 *     tags:
 *       - vendor | registration Process
 *     description: vendor when first time come and need to reigster
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             mobileno:
 *               type: int
 *               example: 1239877415
 *             emailid:
 *               type: string
 *               example: mihirsidhdhapura41@gmail.com
 *             password: 
 *               type: string
 *               example: mihirsensei@41
 *             vendor_name:
 *               type: string
 *               example: Mihir Sidhdhapura
 *             vendor_description:
 *               type: string
 *               example: Nikes parter of product
 *             shop_cover_img:
 *               type: string
 *               example: https/otm-line/s3-bucket/vendor/shop_cover_img.jpg
 *             vendor_profile_img:
 *               type: string
 *               example: https/otm-line/s3-bucket/vendor/shop_profile _img.jpg
 *             address_proof_img:
 *               type: string
 *               example: https/otm-line/s3-bucket/vendor/address_proof _img.jpg
 *             cancel_check_img:
 *               type: string
 *               example: https/otm-line/s3-bucekt/vendor/cancel_check_img.jpg
 *             shop_name:
 *               type: string
 *               example: otm store
 *             aadhar_card:
 *               type: object
 *               properties: 
 *                  aadhar_card_number:
 *                      type: int
 *                      example: 831132382823
 *                  DOB:
 *                      type: string
 *                      format: date
 *                      example: "2000-02-20"
 *                  aadhar_card_name:
 *                      type: string
 *                      example: "Mihir Sidhdhapura"
 *             bank_detail:
 *               type: object
 *               properties: 
 *                  bank_name:
 *                      type: string
 *                      example: "sbi"
 *                  bank_ifsc_code:
 *                      type: string
 *                      example: "SBIN0011050"
 *                  account_number:
 *                      type: string
 *                      example: "SBI69327483"
 *                  account_holder_name:
 *                      type: string
 *                      example: "Mihir "      
 *             shop_address:
 *               type: string
 *               example: Vraj bhumi surat
 *             gstno: 
 *               type: string
 *               example: 12AAACI1681G1Z0
 *             aadhar_card_front_img:
 *               type: string
 *               example: https/otm-lin/s3-bucket/vendor/aadhar_card_front_img.jpg
 *             aadhar_card_back_img:
 *               type: string
 *               example: https/otm-lin/s3-bucket/vendor/aadhar_card_front_img.jpg
 *             location:
 *               type: object
 *               properties:
 *                  city:
 *                      type: string
 *                      example: Surat
 *                  state:
 *                      type: string
 *                      example: Gujarat
 *                  pincode: 
 *                      type: string
 *                      example: 395006
 *                         
 *     responses:
 *       200:
 *         description: Registration sucessfully
 *       500:
 *         description: SERVER ERROR
 */

route.post ('/vendor_register', vendorAuth.vendor_register);

/**
 * @swagger
 * /vendor/vendor_login:
 *   post:
 *     tags:
 *       - vendor | login process
 *     description: vendor login
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              mobileno:
 *                  type: int
 *                  example: 1234567891
 *              password:
 *                  type: string
 *                  example: mihirsensei
 *     responses:
 *          200:
 *              description: login successfully
 *          500: 
 *              description: SERVER ERROR 
 *          
 *          
 *       
 *   
 */
route.post ('/vendor_login', vendorAuth.vendor_login);

/**
 * @swagger
 * /vendor/verify_mobile:
 *   post:
 *     tags:
 *       - vendor | registration Process 
 *     description: check vendor mobile no is exist or not
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              mobileno:
 *                  type: int
 *                  example: 1234567891
 *     responses:
 *          200:
 *              description: vendor mobile data fetch successfully
 *          500: 
 *              description: SERVER ERROR 
 *          
 *          
 *       
 *   
 */

route.post ('/verify_mobile', vendorAuth.verify_mobile);
// need to verify
/**
 * @swagger
 * /vendor/vendor_img_upload:
 *   post:
 *     tags:
 *       - vendor | registration Process
 *     summary: vendor image upload for registration process
 *     description: image upload to s3bucket and return path of image. note give img name of file
 *     produces:
 *       - application/json
 *     consumes:
 *       - multipart/form-data
 *     parameters:
 *       - name: img
 *         in: formData
 *         description: image upload file
 *         type: file
 *       - name: body
 *         in: body
 *         schema:
 *          type: object
 *          properties:
 *              usertype:
 *                  type: string
 *                  example: "vendor"
 *                  
 *     responses:
 *          200:
 *              description: vendor mobile data fetch successfully
 *          500: 
 *              description: SERVER ERROR   
 *   
 */

route.post (
  '/vendor_img_upload',
  uploadImg.upload.single ('img'),
  vendorImageUpload,
  vendorAuth.vendor_image_upload
);

/**
 * @swagger
 * /vendor/vendor_get_profile_status:
 *   post:
 *     tags:
 *       - vendor | registration Process
 *     description: vendor status 
 *     
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              mobileno:
 *                  type: int
 *                  example: 1239877475
 *     responses:
 *          200:
 *              description: vendor status fetch successfully
 *          401:
 *              description: token is missing
 *          500: 
 *              description: SERVER ERROR   
 *   
 */
route.post (
  '/vendor_get_profile_status',
  verifyAccessToken,
  vendorAuth.vendor_get_profile_status
);

route.post ('/verify_mobile', vendorAuth.verify_mobile);
// route.post("/uploadImgArray",uploadImg.upload.array("img"),  vendorAuth.uploadImg)
route.post (
  '/uploadMulImg',
  uploadImg.upload.array ('img'),
  vendorAuth.uploadMuleImg
);

/** 
* @swagger
* /vendor/edit_vender/{id}:
*   put: 
*     tags:
*       - vendor | registration Process
*     description: edit vendor 
*     parameters:
*       - name: body
*         in: body
*         schema: 
*           type: object
*           properties: 
*              id:
*                  type: string
*                  example: 60ec2cb9ad3c4d388c4c4c96    
*     produces:
*       - application/json
*     responses:
*          200:
*              description: edit vendor successfully
*          500: 
*              description: SERVER ERROR   
*   
*/

route.put ('/edit_vender/{id}', vendorAuth.edit_vender);

/** 
* @swagger
* /vendor/get_vendor_status:
*   post: 
*     tags:
*       - vendor | registration Process
*     summary: get vendot status pending/accept/reject
*     description: get vendor status of vendor which is rejected, pending or accepted. its' post
*     parameters:
*       - name: body
*         in: body
*         schema: 
*           type: object
*           properties: 
*              id:
*                  type: string
*                  example: 60ec2cb9ad3c4d388c4c4c96      
*     produces:
*       - application/json
*     responses:
*          200:
*              description: vendor fetch successfully
*          500: 
*              description: SERVER ERROR   
*   
*/

route.post ('/get_vendor_status', vendorAuth.get_vendor_status);

/** 
 * @swagger
 * /vendor/set_rejected_vendor/{id}:
 *   post: 
 *     tags:
 *       - vendor | registration Process
 *     summary: get vendot status pending/accept/reject
 *     description: get vendor status of vendor which is rejected, pending or accepted. its' post
 *     parameters:
 *       - name: id
 *         in: path
 *         required: true
 *         summary: vendor id which one you want edit
 *       - name: body
 *         in: body
 *         description: |
 *          ========================================
 *          for aadhar_card: ==> 
 *          aadhar_card: 
 *              {
 *                  aadhar_card_number:   8328297854257
 *                  DOB:   "2000-02-20" | yyyy-mm-dd
 *                  aadhar_card_name:  "Mihir Sidhdhapura"
 *              }
 *              aadhar_card_front_img: "aadhar_card_new.jpg", 
 *              aadhar_card_back_img: "aadahr_card_back_new.jpg"
 *              
 *          =========================================
 *          for address_proof: ==>
 *              address_proof_img: "image.jpg" 
 *          =========================================
 *         schema: 
 *           type: object
 *           properties: 
 *              bank_detail:
 *               type: object
 *               properties:
 *                  bank_name:
 *                      type: string
 *                      example: "sbi"
 *                  bank_ifsc_code:
 *                      type: string
 *                      example: "SBIN0011050"
 *                  account_number:
 *                      type: string
 *                      example: "SBI69327483"
 *                  account_holder_name:
 *                      type: string
 *                      example: "Mihir updated bank details"    
 *     produces:
 *       - application/json
 *     responses:
 *          200:
 *              description: vendor fetch successfully
 *          500: 
 *              description: SERVER ERROR   
 *   
 */

route.post ('/set_rejected_vendor/:id', vendorAuth.set_rejected_vendor);

/** 
* @swagger
* /vendor/get_pincode:
*   get: 
*     tags:
*       - vendor | registration Process
*     description: get pincode 
*     
*     produces:
*       - application/json
*     responses:
*          200:
*              description: pincode fetch successfully
*          500: 
*              description: SERVER ERROR   
*   
*/

route.get ('/get_pincode', vendorAuth.get_pincode);

/**
 * @swagger
 * /vendor/send_otp:
 *   post:
 *     tags:
 *       - vendor | registration Process
 *       - vendor | login process
 *     description: send otp in mobile no in vendor side
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              mobileno:
 *                  type: int
 *                  example: 1234567895
 *     responses:
 *          200:
 *              description: otp vendor send successfully
 *          500: 
 *              description: SERVER ERROR   
 *   
 */

route.post ('/send_otp', vendorAuth.send_vendor_otp);

/**
 * @swagger
 * /vendor/verify_otp:
 *   post:
 *     tags:
 *       - vendor | registration Process
 *       - vendor | login process
 *     description: send otp in mobile no in vendor side
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              mobileno:
 *                  type: int
 *                  example: 1234567895
 *              password: 
 *                  type: string
 *                  example: "6042"
 *     responses:
 *          200:
 *              description: otp vendor send successfully
 *          500: 
 *              description: SERVER ERROR   
 *   
 */

route.post ('/verify_otp', vendorAuth.verify_otp);

/**
 * @swagger
 * /vendor/reset_password:
 *   post:
 *     tags:
 *       - vendor | login process
 *     description: reset password of user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              mobileno:
 *                  type: int
 *                  example: 1234567895
 *              newPassword: 
 *                  type: string
 *                  example: "6042"
 *     responses:
 *          200:
 *              description: otp vendor send successfully
 *          500: 
 *              description: SERVER ERROR   
 *   
 */

route.post ('/reset_password', vendorAuth.reset_password);

module.exports = route;
