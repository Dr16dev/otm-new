
const  verify_vendor  = require("../../controller/admin/vendorPermission");
const { addLocation,editLocation,location_list} = require("../../controller/admin/addLocation");
const route = require("express").Router();




/**
 * @swagger
 * /admin/verify_vendor:
 *   post:
 *     tags:
 *       - admin | vendor registration process
 *     description: check vendor mobile no is exist or not
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              mobileno:
 *                  type: int
 *                  example: 1234567891
 *     responses:
 *          200:
 *              description: 
 *          500: 
 *              description: SERVER ERROR 
 */

route.post("/verify_vendor", verify_vendor.verify_vendor_list)


route.get("/search_vender",verify_vendor.search_vender)

/**
 * @swagger
 * /admin/verify_vender_list:
 *   get:
 *     tags:
 *       - admin
 *     description: verify vendor list
 *     produces:
 *       - application/json
 *     responses:
 *          200:
 *              description: get activated vendor list
 *          500: 
 *              description: SERVER ERROR 
 */

route.get("/verify_vender_list",verify_vendor.verify_vender_active_list)
/**
 * @swagger
 * /admin/addLocation:
 *   post:
 *     tags:
 *       - admin
 *     description: verify vendor list
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              pincode:
 *                  type: string
 *                  example: 395006
 *              area: 
 *                  type: string
 *                  example: aavadh
 *     responses:
 *          200:
 *              description: get activated vendor list
 *          500: 
 *              description: SERVER ERROR 
 */

route.post("/addLocation", addLocation )

/** 
* @swagger
* /admin/editLocation/{id}:
*   put: 
*     tags:
*       - admin
*     description: edit location 
*     parameters:
*       - name: body
*         in: body
*         schema: 
*           type: object
*           properties: 
*              id:
*                  type: string
*                  example: 60ec2cb9ad3c4d388c4c4c96    
*     produces:
*       - application/json
*     responses:
*          200:
*              description: Location data edit successfully
*          400: 
*              description: something went wrong  
*   
*/


route.put("/editLocation/:id",editLocation)
// route.post("/verify_vendor", verify_vendor.verify_vendor)
/** 
* @swagger
* /admin/location_list:
*   get: 
*     tags:
*       - admin 
*     description: get location 
*     
*     produces:
*       - application/json
*     responses:
*          200:
*              description: Location fetch  successfully
*          400 : 
*               description: something went  wrong!!
*          500: 
*              description: SERVER ERROR   
*   
*/

route.get("/location_list/:id",location_list)
/**
 * @swagger
 * /admin/verify_status:
 *   post:
 *     tags:
 *       - admin | vendor registration process
 *     description: verify status
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema: 
 *           type: object
 *           properties: 
 *              type:
 *                  type: int
 *                  example: 1
 *              mobileno: 
 *                   type: int
 *                   example: 1239877475
 *              status:     
 *                   type: string  
 *                   example: "reject"
 *              reason:
 *                   type: string
 *                   example: "face is not clear"
 * 
 *     responses:
 *          200:
 *              description: get activated vendor list
 *          500: 
 *              description: SERVER ERROR 
 */


route.post("/verify_status",verify_vendor.verify_status)


/**
 * @swagger
 * /admin/search_vendor:
 *   get:
 *     tags:
 *       - admin
 *     summary: search vendor by therir name
 *     description: search vendor by their name
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: vendor_name
 *         in: query
 *         schema: 
 *           type: object
 *     responses:
 *          200:
 *              description: get vendor successfully
 *          500: 
 *              description: SERVER ERROR    
 *   
 */


route.get("/search_vendor", verify_vendor.search_vendor)

// route.get("/get_rejected_vender", )


module.exports = route  

