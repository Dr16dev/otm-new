const express = require("express")
const route = express.Router()

const { view_pending_vendor } = require("../../controller/admin/vendorManager");

/**
 * @swagger
 * /admin/vendor_manager/view_pending_vendor:
 *   post:
 *     tags:
 *       - admin | vendor Management
 *     summary: get pending vendor list of vendor here
 *     description: get pending venod rlsit of vendor here
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: vendor_name
 *         in: query
 *         schema: 
 *           type: object
 *     responses:
 *          200:
 *              description: pending list of vendor
 *          500: 
 *              description: SERVER ERROR      
 *   
 */
route.post("/view_pending_vendor", view_pending_vendor)


module.exports = route
