
const route = require("express").Router();
let  category  = require("../../controller/admin/category");

// category opration
route.get("/all_category_list",category.all_category_list)
route.get("/category_list_by_id/:id",category.category_list_by_id)
route.post("/add_category",category.add_category)

module.exports = route