  const express = require("express");
const jwt = require("jsonwebtoken");

require("dotenv").config();

const Router = new express.Router();
const app = express();
const bodyparser = require("body-parser");

const path = require("path");
const multer = require("multer");

//const mongo = require('./util/mongodb').mongoconnect;
const mongoose = require("mongoose");

mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
//app.use(express.static(__dirname + "/public"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//import routes
const mainroutes = require("./routes")
app.use("/",mainroutes)

// swagagger implemenantion

const swaggerJsonDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "OTM Line API Documention",
      description:
        "OTM Line API procee wise showing below.. for query ask Mihir",
      servers: ["http://localhost:3000&#39;"],
    },
    components: {
      schemas: {
        vendor_auth: {
          properties: {
            mobileno: {
              type: "number",
            },
            vendor_name: {
              type: "string",
            },
            emailid: {
              type: "string",
            },
            password: {
              type: "string",
            },
            shop_cover_image: {
              type: "file",
            },
            shop_address: {
              type: "string",
            },
            address_proof_img: {
              type: "string",
            },
          },
        },
      },
    },
  },
  apis: [
    "./routes/vendor/authVendor.js", 
    "./routes/admin/verifyVendor.js",
    "./routes/admin/vendorManager.js"
  ],
};

const swaggerDocs = swaggerJsonDoc(swaggerOptions);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocs));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization"
  );

  //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  next();
});

cors = require("cors");
app.use(cors());

// mongo(()=>{
//   console.log("this is callback");
// });
// mongodb+srv://bk:Biks00789@cluster0.mt9sa.mongodb.net/<dbname>?retryWrites=true&w=majority
// mongodb://localhost:27017/my_database1
mongoose
  .connect(
    process.env.MONGODB_CONNECTION_STRING ||
      "mongodb://localhost:27017/otmline",
    { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false }
  )
  .then((result) => {
    console.log("Mongo Connected!!!");
    const server = app.listen(process.env.PORT || 3000);

    // process.env.PORT ||
    // const io = require('socket.io')(server);
    // console.log("Client connected");
    // io.on('connection', socket =>
    // {
    //   console.log("soket connected!!!");
    // });
  })
  .catch((err) => {
    console.log(err);
  });

//routing


