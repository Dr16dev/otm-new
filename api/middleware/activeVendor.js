const vendor_auth = require("../model/vendor_auth")
const { responseMiddleWares } = require("./responseMiddleware")

exports.activeVendor = async (req, res, next) => {
    try{
        vendor_auth
        .findOne({_id: req.body.user_id, is_deleted:false, vendor_status: "active"})
        .then(async (activeV) => {
            if(activeV) {
                next()
            }else{
                return res.send(responseMiddleWares("vendor might be not active or deleted please contact admin", false, undefined, 409))
            }
        })

    }catch(err){
        return res.send(responseMiddleWares("something went wrong please contact admin", false, err, 400))
    }
    
}