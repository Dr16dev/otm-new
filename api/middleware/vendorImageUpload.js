require("dotenv").config();
const { responseMiddleWares } =  require("./responseMiddleware");


const sharp = require("sharp");
const aws = require("aws-sdk");

exports.vendorImageUpload = async (req, res, next) =>  {
    try {
        // s3 configuration
        const bucketName = process.env.AWS_BUCKET_NAME
        const s3 = new aws.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY,
            accessSecretKey: process.env.AWS_SECRET_KEY,
            region: process.env.AWS_REGION
        })
        const { file } = req
        await sharp(file.path)
            .resize(170,170,{
                fit:"contain",
                position:"right top"
            })
            .toBuffer()
            .then(bufferData => {
                let uploadParams = {
                    Bucket: bucketName,
                    Body: bufferData,
                    Key: `vendor/smallSize/${file.filename}`
                }
                s3.upload(uploadParams)
                  .promise()
                  .then(data => {
                      console.log("data from s3 bucket", data)
                      
                  })
            })

    } catch (err) {
        console.log("what is wrong here", err)
        res.send(responseMiddleWares("something wrong please contact admin", false, err, 400))
    }
}

