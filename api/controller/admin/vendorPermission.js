const { responseMiddleWares } = require("../../middleware/responseMiddleware");
const vendor_auth_modal = require("../../model/vendor_auth");
const vendor_img = require("../../model/vendor_img");
const vendorauthservices = require("../../services/admin/vendorpermission")

var venderauthobject = new vendorauthservices()

exports.verify_vendor_list = async (req, res) => {
  try {
    const { aadhar_card, mobileno } = req.body;
    if (aadhar_card && mobileno) {
        venderauthobject.verify_vender_list_find({ mobileno }, { aadhar_card},
        (err, docs) => {
          if (err) {
            res.send("something went wrong", false, err, 400);
          }
          res.send(
            responseMiddleWares(
              "Vendor verify successfully",
              true,
              undefined,
              200
            )
          );
        }
      );
    } else {
      res.send(
        responseMiddleWares(
          "mobileno or aadharcard detail is empty",
          false,
          undefined,
          400
        )
      );
    }
  } catch (err) {
    res.send(
      responseMiddleWares(
        "something went wrong please contacta admin",
        false,
        err,
        400
      )
    );
  }
};

// all done all the edge case is defined
exports.verify_status = async (req, res) => {
  try {
    const { type, mobileno, status } = req.body;
    let updateObj = {};
    switch (type) {
      case 1:
        // aadhar card accept or reject
        if (status) {
          if (status === "reject") {
            updateObj = {
              status_verified: false,
              vendor_status: "pending",
              $set: { "verification.aadhar_card_verified": "reject" },
              $push: {
                rejected_list: {
                  rejected_type: "aadhar",
                  rejected_reason: req.body.reason
                },
              },
            };
          } else if (status === "accept") {
            updateObj = {
              $set: { "verification.aadhar_card_verified": "accept" },
              $pull: {
                rejected_list: {
                  rejected_type: "aadhar",
                },
              },
            };
          } else {
            return res.send(
              responseMiddleWares(
                "please enter the proper status reject or accept",
                false,
                undefined,
                400
              )
            );
          }
        } else
          return res.send(
            responseMiddleWares(
              "please enter proper aadhar_card_status field",
              false,
              undefined,
              400
            )
          );
        break;
      case 2:
        // address proof accept or reject
        if (status) {
          if (status === "reject") {
            updateObj = {
              status_verified: false,
              $set: { "verification.address_proof_verified": "reject" },

              vendor_status: "pending",
              $push: {
                rejected_list: {
                  rejected_type: "address",
                  rejected_reason: req.body.reason,
                },
              },
            };
          } else if (status === "accept") {
            updateObj = {
              $set: { "verification.address_proof_verified": "accept" },
              $pull: {
                rejected_list: {
                  rejected_type: "address",
                },
              },
            };
          } else {
            return res.send(
              responseMiddleWares(
                "please enter the proper status reject or accept",
                false,
                undefined,
                400
              )
            );
          }
        } else
          return res.send(
            responseMiddleWares(
              "please enter proper address_proof_status field",
              false,
              undefined,
              400
            )
          );
        break;
      case 3:
        //bank detail accept or reject
        if (status) {
          if (req.body.status === "reject") {
            updateObj = {
              $set: { "verification.bank_detail_verified": "reject" },

              vendor_status: "pending",
              status_verified: false,
              $push: {
                rejected_list: {
                  rejected_type: "bank",
                  rejected_reason: req.body.reason,
                },
              },
            };// aa km nai chaltu te mare search krvu pdse tamara ma to chale che ne?haa ok jovo services
          } else if (status === "accept") {
            updateObj = {
              $set: { "verification.bank_detail_verified": "accept" },

              $pull: {
                rejected_list: {
                  rejected_type: "bank",
                },
              },
            };
          } else {
            return res.send(
              responseMiddleWares(
                "please enter the proper status reject or accept",
                false,
                undefined,
                400
              )
            );
          }
        } else
          return res.send(
            responseMiddleWares(
              "please enter proper bank_detail_status field",
              false,
              undefined,
              400
            )
          );
        break;
      default:
        updateObj = {};
    }
    // venderauthobject.editlocation(req.params.id, req.body, function(err, docs) {
    let updatedOne = await vendor_auth_modal
      .findOneAndUpdate({ mobileno }, updateObj, { new: true })
      .then(async (data) => {
        if (
          data.verification.aadhar_card_verified == "accept" &&
          data.verification.address_proof_verified == "accept" &&
          data.verification.bank_detail_verified == "accept"
        ) {
          console.log("coming here kid", data._id);
          await vendor_auth_modal.findByIdAndUpdate(
            data._id,
            {
              status_verified: true,
              vendor_status: "active",
            },
            (err, docs) => {
              if (err) {
                return res.send(
                  responseMiddleWares("something wrong", false, err, 400)
                );
              } else {
                console.log("vendor is successfully verified now");
                res.send(
                  responseMiddleWares(
                    "vendor is verified successfully",
                    true,
                    { vendor_id: data._id },
                    200
                  )
                );
                return;
              }
            }
          );
        } else {
          return res.send(
            responseMiddleWares(`verified successfully`, true, updateObj, 200)
          );
        }
      })
      .catch((err) => {
        res.send(
          responseMiddleWares("something please contact admin", false, err, 400)
        );
      });
  } catch (err) {
    res.send(
      responseMiddleWares(
        "something wrong please contact admin",
        false,
        undefined,
        400
      )
    );
  }
};

// exports.view_pending_vendor = async (req, res) => {
//     try {
//         await vendor_auth_modal
//             .find({ vendor_status: "pending", is_deleted: false })
//             .then(vendor => {
//                 if (vendor.length > 0) {
//                     vendor_img
//                             .findOne({vendor_id: vendor._id}, {_id: 0})
//                             .then(vendorimg => {
//                                 if(vendorimg.length > 0){
//                                     return res.send(responseMiddleWares("vendor data extracted", true, {vendor, vendorimg}, 200))

//                                 }else{
//                                     console.log("no data found of images please contact admin")
//                                 }
//                             } )
//                 } else {
//                     return res.send(responseMiddleWares("no pending vendor is exist", false, undefined, 400))
//                 }
//             })
//     } catch (err) {
//         return res.send("something wrong please contact admin", false, undefined, 404)
//     }
// }

exports.verify_vender_active_list = async (req, res) => {
  try {
    await vendor_auth_modal.find({ vendor_status: "active" }).then((vendor) => {
      if (vendor.length > 0) {
        return res.send(
          responseMiddleWares("vendor data extracted", true, vendor, 200)
        );
      } else {
        return res.send(
          responseMiddleWares(
            "no active vendor is exist",
            false,
            undefined,
            400
          )
        );
      }
    });
  } catch (err) {
    return res.send(
      "something wrong please contact admin",
      false,
      undefined,
      404
    );
  }
};
exports.search_vender = async (req, res) => {
  try {
    const data = vendor_auth_modal.find().then((venderdata) => {
      const filters = req.query;
      const filteredUsers = venderdata.filter((user) => {
        let isValid = true;
        for (key in filters) {
          isValid = isValid && user[key] == filters[key];
        }
        return isValid;
      });
      res.send(
        responseMiddleWares("List of vendor data", true, filteredUsers, 200)
      );
    });
  } catch {
    res.send("something wrong please contact admin", false, undefined, 400);
  }
};

exports.search_vendor = async (req, res) => {
  try {
    await vendor_auth_modal
      .find(
        { vendor_name: { $regex: req.query.vendor_name, $options: "i" } },
        {
          vendor_name: 1,
          vendor_profile_img: 1,
          shop_name: 1,
          mobileno: 1,
        }
      )
      .then((data) => {

        if (data.length > 0) {
          res.send(
            responseMiddleWares("Data fetched successfully", true, data, 200)
          );
        } else {
          res.send(
            responseMiddleWares(
              `no data found with the containing ${req.query.vendor_name} try something else`,
              false,
              undefined,
              400
            )
          );
        }
      })
      .catch((err) => {
        return res.send(`something wrong here ${err}`);
      });
  } catch (err) {
    res.send(
      responseMiddleWares(
        "somthing went wrong please contact admin",
        false,
        undefined,
        400
      )
    );
    return;
  }
};
