const { default: validator } = require("validator");
const { responseMiddleWares } = require("../../middleware/responseMiddleware");
const locationModel = require("../../model/location");
const addlocationservices = require("../../services/admin/addlocation")
var addlocationobject = new addlocationservices()

exports.addLocation = async (req, res) => {
    try{
        const { pincode, area } = req.body;
        if(pincode){
            //check pincode is really valid or not
            if(validator.isPostalCode(pincode, "IN")){
                //add to the database
                addlocationobject.getlocation(pincode, function(err, getlocation){
                if(err){
                   return res.send(responseMiddleWares("Something went wrong!!", false, err, 404))          
                }else{
                    if(getlocation.length > 0){
                        return res.send(responseMiddleWares("pincode is already exist try differnt pincode", false, undefined, 400))
                    }
                    else
                    {
                        addlocationobject.addlocation(pincode,area,function(err, addlocation){
                            if(addlocation)
                             { 
                                 return res.send(responseMiddleWares("location added successfully", true, addlocation, 200))
                            }else{
                                return res.send(responseMiddleWares("Location not added", false, err, 400))
                            }
                        })
                    }
                }
            })
            }else{
                return res.send(responseMiddleWares("please entry proper pincode", false, undefined, 400))
            }
           
        }else{
            return res.send(responseMiddleWares("please enter the pincode", false, undefined, 400))
        }
    }catch(err) {
        return  res.send(responseMiddleWares("something wrong please contact admin", false, err, 400))
    }
}                


exports.editLocation = async (req, res) => {
    try{
        const { pincode, area } = req.body;
        addlocationobject.editlocation(req.params.id, req.body, function(err, docs) {
            if (err) {
              return res.send("something went wrong!!", false, err, 400);
            } else {
                console.log("docs --",)
              return res.send(
                  responseMiddleWares("Location data edit successfully", true, docs, 200)
              );
            }
          });
        } catch (err) {
          return res.send(
            responseMiddleWares(
              "something went wrong please contact a admin",
              false,
              undefined,
              404
            )
          );
        }
}

exports.location_list = async (req, res) => {
    try {
        
        addlocationobject.location_list({_id:req.params.id}, function(err, location) {
            if (err) {
                return res.send(responseMiddleWares("something went wrong!!", false, err, 400))
            } else {                
                if(location){
                   return res.send(responseMiddleWares("Location fetch  successfully", true,location, 200))
                }else{
                    return res.send(responseMiddleWares("no record found!!", false, undefined,400))
                }
            }
        }) 
    }catch (error) {
       return res.send(responseMiddleWares("something went wrong please contact a admin", false, error, 404))
    }
}