const { responseMiddleWares } = require("../../middleware/responseMiddleware")
const vendor_auth_modal = require('../../model/vendor_auth');

// exports.view_pending_vendor = async (req, res) => {
//     try {
//         await vendor_auth_modal
//             .find({ vendor_status: "pending", is_deleted: false })
//             .
//             .then(vendor => {
//                 if (vendor.length > 0) {
//                     console.log("venodr data", vendor)
//                     vendor_img
//                             .findOne({vendor_id: vendor._id}, {_id: 0})
//                             .then(vendorimg => {
//                                 if(vendor_img.length > 0){  
//                                     console.log("vendor status: ")
//                                     return res.send(responseMiddleWares("vendor data extracted", true, {vendor, vendorimg}, 200))
                                    
//                                 }else{
//                                     console.log("no data found of images please contact admin")
//                                 }
//                             } )
//                 } else {
//                     return res.send(responseMiddleWares("no pending vendor is exist", false, undefined, 400))
//                 }
//             })
//     } catch (err) {
//         return res.send("something wrong please contact admin", false, undefined, 404)
//     }
// }


exports.view_pending_vendor = async (req, res) => {

    try {
        await vendor_auth_modal
            .aggregate([
                {
                    "$match":{
                        "vendor_status": req.body.status,
                        "is_deleted": false
                    }
                },
                {
                
                    "$lookup":{
                        "from": "vendor_imgs",
                        "localField":"_id",
                        "foreignField":"vendor_id",
                        "as": "vendor_image" 
                    }
                }, 
                {
                    "$project":{
                        "vendor_image._id": 0,
                        "vendor_image.vendor_id":0,
                        "vendor_image.__v":0
                    }

                }
            ])            
            .then(vendor => {
                if (vendor.length > 0) {
                   console.log("vendor data", vendor)
                   res.send(responseMiddleWares("done", true, vendor, 200))
                } else {
                    return res.send(responseMiddleWares("no pending vendor is exist", false, undefined, 400))
                }
            })
    } catch (err) {
        return res.send("something wrong please contact admin", false, undefined, 404)
    }
}