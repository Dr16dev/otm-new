const Category_Schema = require('../../model/category');
const response = require('../../middleware/responseMiddleware');
const { Mongoose } = require('mongoose');
const gstValidator = require("gstin-validator");

const { responseMiddleWares } = require("../../middleware/responseMiddleware");
const categoryservices = require("../../services/admin/category")
var categoryobject = new categoryservices()
// add category 
exports.add_category = async(req,res) => {
    try {
        const {category,sub_category} = req.body;
        if(!category){
            return res.send(response.responseMiddleWares("Category is not define", false, undefined, 400))
        }
      
        categoryobject.addcategory(category,sub_category, function(err, addcategory){
            if(addcategory)
            { 
                return res.send(responseMiddleWares("location added successfully", true, addcategory, 200))
            }else{
                return res.send(response.responseMiddleWares("something wrong!!", false, err, 400))
            }})
    
    }catch(err){
        return res.send(response.responseMiddleWares("something wrong!!", false,err ,400))
    }
}

// list of all category
exports.all_category_list = async (req, res) => {
    try {
        categoryobject.getallcategory( function(err, getcategorylist){
            if (err) {
                return res.send(response.responseMiddleWares("something wrong!!", false, err, 400))
            } else {
                return res.send(response.responseMiddleWares("Success", true,getcategorylist, 200))
            }
        }) 
    }catch (error) {
        return res.send(response.responseMiddleWares("something wrong", false, error, 400))
    }
}


// list of category by id
exports.category_list_by_id = async (req, res) => {
    try {
        await  categoryobject.getcategorybyid({_id:req.params.id}, function(err, category){
            if (err) {
                return res.send(response.responseMiddleWares("something wrong!!", false, err, 400))
            } else {
                return res.send(response.responseMiddleWares("Success", true,category, 200))
            }
        }) 
    }catch (error) {
       return res.send(response.responseMiddleWares("something wrong!!", false, error, 400))
    }
}
