const category = require('../../model/category');
const { responseMiddleWares } = require("../../middleware/responseMiddleware");
const vendor_category_services = require("../../services/vendor/categoryservices")
var vendor_category_object = new vendor_category_services()

exports.search_category = async(req,res,next)=>{
    try {
        var re = new RegExp(req.query.search, 'i')
        await  vendor_category_object.searchcategory(re, function(err, data){
            if (data.length > 0) {
              res.send(
                responseMiddleWares("Data fetched successfully",  true, data, 200)
              );
            } else {
              res.send(
                responseMiddleWares(
                  `no data found with the containing ${req.query.search} try something else`,
                  false,
                  err,
                  400
                )
              );
            }
          })
          
      } catch (err) {
        res.send(
          responseMiddleWares(
            "somthing went wrong please contact admin",
            false,
            undefined,
            404
          )
        );
        return;
      }
}