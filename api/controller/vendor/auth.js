const vendor_auth_modal = require ('../../model/vendor_auth');
const otp_model = require ('../../model/otp');
const response = require ('../../middleware/responseMiddleware');
const {Mongoose} = require ('mongoose');
const {default: validator} = require ('validator');
const gstValidator = require ('gstin-validator');
const jwt = require ('jsonwebtoken');
const {encrypt, decrypt} = require ('../../helper/encry_decry');
const {
  uploadFile,
  uploadReduceFile,
  uploadMulFileS3,
} = require ('../../helper/s3');
const sharp = require ('sharp');
const {responseMiddleWares} = require ('../../middleware/responseMiddleware');
const randomstring = require ('randomstring');
const auth_services = require ('../../services/vendor/auth');
var auth_object = new auth_services ();
const pincodeModel = require ('../../model/location');
const vendor_img = require ('../../model/vendor_img');
const useragent = require ('useragent');
const vendor_auth = require ('../../model/vendor_auth');

require ('dotenv').config ();

exports.vendor_register = async (req, res) => {
  const registerdata = ({
    mobileno,
    emailid,
    vendor_name,
    password,
    aadhar_card,
    aadhar_card_front_img,
    aadhar_card_back_img,
    cancel_check_img,
    address_proof_img,
    shop_cover_img,
    vendor_profile_img,
    shop_address,
    bank_detail,
    gstno,
    shop_name,
    location,
  } = req.body);
  Object.entries (req.body).forEach (([key, value]) => {
    if (value == undefined || value == null || value == '') {
      res.send (
        response.responseMiddleWares (
          `value is missing of ${key}`,
          false,
          undefined,
          400
        )
      );
    }
  });
  if (Number.isInteger (mobileno)) {
    if (mobileno.toString ().length != 10) {
      res.send (
        response.responseMiddleWares (
          'mobile no must be 10',
          false,
          undefined,
          400
        )
      );
      return;
    }
  } else {
    res.send (
      response.responseMiddleWares (
        'Mobile Number not be string',
        false,
        undefined,
        400
      )
    );
    return;
  }
  if (!validator.isEmail (emailid)) {
    res.send (
      response.responseMiddleWares (
        'Email is not proper',
        false,
        undefined,
        400
      )
    );
    return;
  }
  if (!vendor_name) {
    res.send (
      response.responseMiddleWares (
        'vendor name is not define',
        false,
        undefined,
        400
      )
    );
    return;
  }
  if (!password) {
    res.send (
      response.responseMiddleWares (
        'password is not define',
        false,
        undefined,
        400
      )
    );
    return;
  }
  if (!shop_cover_img) {
    res.send (
      response.responseMiddleWares (
        'shop images is not define',
        false,
        undefined,
        400
      )
    );
    return;
  }
  if (!vendor_profile_img) {
    res.send (
      response.responseMiddleWares (
        'vendor profile image is not define',
        false,
        undefined,
        400
      )
    );
    return;
  }
  if (!shop_address) {
    res.send (
      response.responseMiddleWares (
        'shop_address is not define',
        false,
        undefined,
        400
      )
    );
    return;
  }

  //gst validation
  if (gstno) {
    if (!gstValidator.isValidGSTNumber (gstno.toString ())) {
      res.send (
        response.responseMiddleWares (
          'gst number is not valid',
          false,
          undefined,
          400
        )
      );
      return;
    }
  } else {
    res.send (
      response.responseMiddleWares (
        'gstno is not define',
        false,
        undefined,
        400
      )
    );
    return;
  }

  if (!shop_name) {
    res.send (
      response.responseMiddleWares (
        'shop_name is not define',
        false,
        undefined,
        400
      )
    );
    return;
  }

  let userExist = await vendor_auth_modal.findOne ({
    mobileno: req.body.mobileno,
  });
  if (userExist == undefined) {
    await auth_object.save_vendor_register (registerdata, function (err, data) {
      console.log ('data', data, err);
      if (data) {
        auth_object.save_register_image (data, function (err, saveimage) {
          if (saveimage) {
            res.send (
              response.responseMiddleWares (
                'Vendor Registration done',
                true,
                {vendor_id: data._id},
                200
              )
            );
          } else {
            res.send (
              response.responseMiddleWares ('something wrong', false, err, 400)
            );
            return;
          }
        });
      } else {
        res.send (
          response.responseMiddleWares ('something wrong', false, err, 400)
        );
        return;
      }
    });
  } else {
    res.send (
      response.responseMiddleWares (
        'This Mobile is already exist please try another mobile no',
        false,
        undefined,
        409
      )
    );
    return;
  }
};

exports.vendor_image_upload = async (req, res) => {
  try {
    // const { file } = req
    // let resObj = {}
    if (req.body.usertype == 'vendor') {
      let result = uploadFile (req.file, 'vendor', 'originalSize');
      await result
        .then (data => {
          return res.send (
            responseMiddleWares (
              'img upload successfully',
              true,
              data.Location,
              200
            )
          );
        })
        .catch (err => {
          return res.send (
            responseMiddleWares (
              'something wrong contact admin',
              false,
              err,
              400
            )
          );
        });
    } else {
      return res.send (responseMiddleWares ("didn't set up for other user"));
    }

    // for(const [index,image] of files.entries()){
    //     console.log("something here  in before result")

    //     let result = uploadFile(image, "vendor", "OriginalSize")

    //     console.log("something here  in ater result")
    //     await result
    //         .then( data => {
    //             return res.send(responseMiddleWares("Img uploaded successfully",  true, data.Location, 200))
    //         })
    //         .catch(err => {
    //             console.log(err)
    //             return res.send(err)
    //         })

    // }
    // res.send(resObj)
  } catch (err) {
    res.send (
      response.responseMiddleWares (
        'something worng please contact admin',
        false,
        err,
        400
      )
    );
  }
};

exports.vendor_login = async (req, res) => {
  try {
    let brow = useragent.parse (req.headers['user-agent']).toString ();
    var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null;
    console.log ('user agent ', req.headers['user-agent']);
    const {mobileno, password} = req.body;

    vendor_auth_modal.findOne ({mobileno: mobileno, password: password}).then (
      vendor_data => {
        if (vendor_data) {
          // if (vendor_data.password === password) {
          vendor_auth_modal
            .findByIdAndUpdate (
              vendor_data._id,
              {
                $push: {
                  browser: {
                    ip_address: ip,
                    user_client: brow,
                  },
                },
              },
              {new: true}
            )
            .then (data => {
              console.log ('ip addeed sucessfully ;) ', data);
            });
          let encryUser = encrypt (vendor_data._id);
          token = jwt.sign ({user_id: encryUser}, process.env.ACCESS_TOKEN, {
            expiresIn: process.env.ACCESS_TIME,
          });
          res.send (
            response.responseMiddleWares (
              'login successful',
              true,
              {token},
              200
            )
          );
          return;
        } else {
          // else {
          //   res.send(
          //     response.responseMiddleWares(
          //       "mobile number and password wrong!!",
          //       false,
          //       undefined,
          //       403
          //     )
          //   );
          //   return;
          // }
          res.send (
            response.responseMiddleWares (
              'mobile number and password wrong!!',
              false,
              undefined,
              403
            )
          );
        }
      },
      err => {
        res.send (
          response.responseMiddleWares ("can't find the user", false, err, 400)
        );
        return;
      }
    );
  } catch (err) {
    res.send (
      response.responseMiddleWares ('something wrong', false, err, 400)
    );
    return;
  }
};

exports.vendor_register_otp_verify = async (req, res, next) => {
  try {
  } catch (error) {}
};

exports.vendor_get_profile_status = async (req, res) => {
  try {
    //send vendor profile status
    await vendor_auth_modal.findById (
      decrypt (req.body.user_id),
      (err, vendor) => {
        if (err) {
          res.send (err);
        } else {
          if (!vendor.is_deleted) {
            res.send (
              response.responseMiddleWares (
                'sucess',
                true,
                {status: vendor.vendor_status},
                200
              )
            );
          } else {
            res.send (
              response.responseMiddleWares (
                'vendor is deleted',
                false,
                err,
                400
              )
            );
          }
        }
      }
    );
  } catch (error) {
    res.send (
      response.responseMiddleWares ('something wrong', false, undefined, 404)
    );
  }
};

exports.uploadCompressFile = async (req, res) => {
  try {
    console.log (req.file);
    let result = await uploadReduceFile (req.file);
    res.send (result);
  } catch (err) {
    res.send (err);
  }
};

exports.uploadSingleImg = async (req, res) => {
  try {
    const file = req.file;
    //upload function from s3

    let result = await uploadFile (file, 'vendor', 'originalSize');

    res.send ({message: 'profile image uploaded successfully'});

    // const result = await uploadFile(file, "vendor", "originalSize")
    // console.log("from s3 bucket")
    // console.log(result);
    // res.send(result)
  } catch (err) {
    console.log (err);
    res.send (
      response.responseMiddleWares ('something wrong', false, err, 400)
    );
  }
};

exports.uploadMuleImg = (req, res) => {
  req.files.forEach (async (images, index) => {
    let resut = await uploadMulFileS3 (images, req.body.type, req.body.size);

    if (req.files.length - 1 == index) {
      res.send ('uploaded successfully');
    }
  });
};

//verrify mobile
exports.verify_mobile = async (req, res) => {
  try {
    if (Number.isInteger (req.body.mobileno)) {
      if (req.body.mobileno.toString ().length != 10) {
        res.send (
          response.responseMiddleWares (
            'mobile no must be 10',
            false,
            undefined,
            400
          )
        );
      }
    }

    await vendor_auth_modal.findOne (
      {mobileno: req.body.mobileno},
      (err, data) => {
        if (err) {
          console.log (err);
          res.send (
            response.responseMiddleWares (
              'something wrong contact admin',
              false,
              undefined,
              400
            )
          );
        } else {
          if (data === null) {
            res.send (
              response.responseMiddleWares (
                'mobile number is not exist',
                true,
                undefined,
                200
              )
            );
          } else {
            res.send (
              response.responseMiddleWares (
                'mobile is exist try another number',
                false,
                undefined,
                400
              )
            );
          }
        }
      }
    );
  } catch (err) {
    res.send (
      response.responseMiddleWares (
        'something wrong conctact admin',
        false,
        err,
        404
      )
    );
  }
};

exports.edit_vender = async (req, res) => {
  try {
    const id = req.params.id;

    Object.entries (req.body).forEach (([key, value]) => {
      if (value == undefined || value == null || value == '') {
        res.send (
          response.responseMiddleWares (
            `value is missing of ${key}`,
            false,
            undefined,
            400
          )
        );
      }
    });

    if (Number.isInteger (mobileno)) {
      if (mobileno.toString ().length != 10) {
        res.send (
          response.responseMiddleWares (
            'mobile no must be 10',
            false,
            undefined,
            400
          )
        );
      }
    } else {
      res.send (
        response.responseMiddleWares (
          'Mobile Number not be string',
          false,
          undefined,
          400
        )
      );
    }
    if (!validator.isEmail (emailid)) {
      res.send (
        response.responseMiddleWares (
          'Email is not proper',
          false,
          undefined,
          400
        )
      );
    }
    if (!vendor_name) {
      res.send (
        response.responseMiddleWares (
          'vendor name is not define',
          false,
          undefined,
          400
        )
      );
    }
    if (!password) {
      res.send (
        response.responseMiddleWares (
          'password is not define',
          false,
          undefined,
          400
        )
      );
    }
    if (!shop_cover_image) {
      res.send (
        response.responseMiddleWares (
          'shop images is not define',
          false,
          undefined,
          400
        )
      );
    }
    if (!shop_profile_image) {
      res.send (
        response.responseMiddleWares (
          'shop profile images is not define',
          false,
          undefined,
          400
        )
      );
    }
    if (!shop_address) {
      res.send (
        response.responseMiddleWares (
          'shop_address is not define',
          false,
          undefined,
          400
        )
      );
    }

    //gst validation
    if (gstno) {
      if (!gstValidator.isValidGSTNumber (gstno.toString ())) {
        res.send (
          response.responseMiddleWares (
            'gst number is not valid',
            false,
            undefined,
            400
          )
        );
      }
    } else {
      res.send (
        response.responseMiddleWares (
          'gstno is not define',
          false,
          undefined,
          400
        )
      );
    }

    if (!ip_address) {
      res.send (
        response.responseMiddleWares (
          'ip_address is not define',
          false,
          undefined,
          400
        )
      );
    }
    if (!shop_name) {
      res.send (
        response.responseMiddleWares (
          'shop_name is not define',
          false,
          undefined,
          400
        )
      );
    }
    if (!city) {
      res.send (
        response.responseMiddleWares (
          'city is not define',
          false,
          undefined,
          400
        )
      );
    }
    if (!state) {
      res.send (
        response.responseMiddleWares (
          'state is not define',
          false,
          undefined,
          400
        )
      );
    }
    if (!pincode) {
      res.send (
        response.responseMiddleWares (
          'pincode is not define',
          false,
          undefined,
          400
        )
      );
    }
    const {
      mobileno,
      emailid,
      vendor_name,
      password,
      shop_cover_image,
      shop_profile_image,
      shop_address,
      gstno,
      ip_address,
      shop_name,
      city,
      state,
      pincodes,
    } = req.body;

    auth_object.editvendor (id, req.body, function (err, docs) {
      if (err) {
        return res.send ('something went wrong', false, err, 400);
      } else {
        return res.send (
          responseMiddleWares ('Vendor data edit successfully', true, docs, 200)
        );
      }
    });
  } catch (err) {
    return res.send (
      responseMiddleWares (
        'something went wrong please contacta admin',
        false,
        undefined,
        404
      )
    );
  }
};

exports.get_vender_pincode = async (req, res) => {
  try {
    await auth_object.find_vender_pincode ((err, docs) => {
      if (err) {
        return res.send (
          response.responseMiddleWares ('something wrong!!', false, err, 400)
        );
      } else {
        let pincode = data.map (data => data.pincode);
        return res.send (
          response.responseMiddleWares ('success', true, pincode, 200)
        );
      }
    });
  } catch (err) {
    res.send (
      response.responseMiddleWares (
        'something wrong conctact admin',
        false,
        undefined,
        404
      )
    );
  }
};

// generate uniq code

exports.send_vendor_otp = async (req, res) => {
  try {
    if (req.body.mobileno) {
      if (req.body.mobileno.toString ().length != 10) {
        return res.send (
          response.responseMiddleWares (
            'mobile no must be 10',
            false,
            undefined,
            400
          )
        );
      }
    }

    var u_code = randomstring.generate ({
      length: 4,
      charset: 'numeric',
    });
    (mobileno =
      req.body
        .mobileno), (otp = u_code), await auth_object.send_otp_save (
      mobileno,
      otp,
      function (err, data) {
        if (data) {
          return res.send (
            response.responseMiddleWares (
              'otp create successfully!!',
              true,
              data,
              200
            )
          );
        } else {
          return res.send (
            response.responseMiddleWares ('something wrong!!', false, err, 400)
          );
        }
      }
    );
  } catch (err) {
    console.log ('err', err);
    return res.send (
      response.responseMiddleWares (
        'something wrong conctact admin',
        false,
        err,
        404
      )
    );
  }
};

//rejected vender data
exports.get_vendor_status = async (req, res) => {
  try {
    await vendor_auth_modal
      .findById (req.body.id)
      .exec ()
      .then (async vendor => {
        if (vendor != null) {
          let vendorImg = {};
          await vendor_img
            .find ({vendor_id: vendor._id})
            .then (vendorImgData => (vendorImg = vendorImgData[0]));
          let resObj = {
            vendor_id: vendor._id,
            mobileno: vendor.mobileno,
          };
          for (let i = 0; i < 3; i++) {
            switch (i) {
              case 0:
                {
                  if (vendor.aadhar_card_verified == 'reject') {
                    let aadhar_card = {
                      aadhar_card_detail: vendor.aadhar_card,
                      rejected_reason: vendor.rejected_list
                        .reverse ()
                        .find (list => list.rejected_type == 'aadhar')
                        .rejected_reason,
                      aadhar_card_front_img: vendorImg.aadhar_card_front_img,
                      aadhar_card_back_img: vendorImg.aadhar_card_back_img,
                    };
                    resObj['aadhar_card_verified'] = 'reject';
                    resObj['aadhar_card_detail'] = aadhar_card;
                  } else if (vendor.aadhar_card_verified == 'accept') {
                    resObj['aadhar_card_verified'] = 'accept';
                  } else if (vendor.aadhar_card_verified == 'pending') {
                    resObj['aadhar_card_verified'] = 'pending';
                  } else {
                    return res.send (
                      responseMiddleWares (
                        'vendor aadhar card verified status is not proper contact admin',
                        false,
                        undefined,
                        400
                      )
                    );
                  }
                }
                break;
              case 1:
                if (vendor.bank_detail_verified == 'reject') {
                  let bank_detail = {
                    bank_details: vendor.bank_detail,
                    rejected_reason: vendor.rejected_list
                      .reverse ()
                      .find (list => list.rejected_type == 'bank')
                      .rejected_reason,
                    cancel_check_img: vendorImg.cancel_check_img,
                  };
                  resObj['bank_detail_verified'] = 'reject';
                  resObj['bank_detail'] = bank_detail;
                } else if (vendor.bank_detail_verified == 'accept') {
                  resObj['bank_detail_verified'] = 'accept';
                } else if (vendor.bank_detail_verified == 'pending') {
                  resObj['bank_detail_verified'] = 'pending';
                } else {
                  return res.send (
                    'vendor bank detail verifed status is not proper contact admin',
                    false,
                    undefined,
                    400
                  );
                }
                break;
              case 2:
                if (vendor.address_proof_verified == 'reject') {
                  let address_proof = {
                    address_proof_img: vendorImg.address_proof_img,
                    rejected_reason: vendor.rejected_list
                      .reverse ()
                      .find (list => list.rejected_type == 'address')
                      .rejected_reason,
                  };
                  resObj['address_proof_verified'] = 'reject';
                  resObj['address_proof'] = address_proof;
                } else if (vendor.address_proof_verified == 'accept') {
                  resObj['address_proof_verified'] = 'accept';
                } else if (vendor.address_proof_verified == 'pending') {
                  resObj['address_proof_verified'] = 'pending';
                } else {
                  return res.send (
                    responseMiddleWares (
                      'vendor address proof verified status is not proper',
                      false,
                      undefined,
                      400
                    )
                  );
                }
                break;
            }
          }
          return res.send (responseMiddleWares ('Success', true, resObj, 200));
        } else {
          return res.send (
            responseMiddleWares ('no rejected vendor is exist', false, err, 404)
          );
        }
      });
  } catch (err) {
    return res.send (
      'something wrong please contact admin',
      false,
      undefined,
      404
    );
  }
};
exports.set_rejected_vendor = async (req, res) => {
  try {
    console.log (req.params);
    await vendor_auth_modal
      .findOne ({_id: req.params.id, status_verified: false})
      .then (async vendor => {
        if (vendor) {
          const {
            bank_detail,
            address_proof_img,
            cancel_check_img,
            aadhar_card,
            aadhar_card_back_img,
            aadhar_card_front_img,
          } = req.body;
          let vendor_edit = {
            bank_detail,
            aadhar_card,
            vendor_status: 'pending',
            status_verified: false,
            is_deleted: false,
          };
          await vendor_auth_modal.findByIdAndUpdate (
            req.params.id,
            vendor_edit,
            async (err, data) => {
              if (err) {
                return res.send (
                  responseMiddleWares ('something wrong!!', false, err, 400)
                );
              } else {
                await vendor_img.findOneAndUpdate (
                  {vendor_id: req.params.id},
                  {
                    aadhar_card_front_img,
                    aadhar_card_back_img,
                    cancel_check_img,
                    address_proof_img,
                  },
                  (err, imgData) => {
                    if (err) {
                      return res.send (
                        responseMiddleWares (
                          'something wrong in image part',
                          false,
                          undefined,
                          400
                        )
                      );
                    } else {
                      return res.send (
                        responseMiddleWares (
                          'Update data successfully!!',
                          true,
                          data._id,
                          200
                        )
                      );
                    }
                  }
                );
              }
            }
          );
        } else {
          return res.send (
            responseMiddleWares (
              'no rejected vendor is exist!!',
              false,
              err,
              400
            )
          );
        }
      });
  } catch (err) {
    return res.send (
      responseMiddleWares (
        'something wrong please contact admin',
        false,
        undefined,
        404
      )
    );
  }
};

exports.verify_otp = async (req, res) => {
  try {
    otp_model
      .findOne (req.body)
      .then (data => {
        if (data !== null) {
          return res.send (
            responseMiddleWares ('Mobileno verified !!', true, data, 200)
          );
        } else {
          return res.send (
            responseMiddleWares ('No data found !!', false, err, 400)
          );
        }
      })
      .catch (err => {
        return res.send (
          responseMiddleWares (
            'Please check your mobile number and otp!!',
            false,
            err,
            400
          )
        );
      });
  } catch (err) {
    return res.send (
      responseMiddleWares (
        'something wrong please contact admin',
        false,
        err,
        404
      )
    );
  }
};

exports.get_pincode = async (req, res) => {
  try {
    await pincodeModel
      .find ({}, {pincode: 1, _id: 0})
      .then (data => {
        if (data.length > 0) {
          pincodeData = data.map (pin => pin.pincode);
          return res.send (
            responseMiddleWares (
              'pincode extracted',
              true,
              {pincode: pincodeData},
              200
            )
          );
        } else {
          return res.send (
            responseMiddleWares (
              'currently no pincode added to work with',
              false,
              undefined,
              400
            )
          );
        }
      })
      .catch (err => {
        return res.send (err);
      });
  } catch (err) {
    return res.send (
      responseMiddleWares (
        'something wrong please contact admin',
        false,
        undefined,
        400
      )
    );
  }
};

exports.reset_password = (req, res) => {
  try {
    const mobileno = req.body.mobileno;
    const password = req.body.newPassword;
    auth_object.reset_password_update (mobileno, password, function (
      err,
      data
    ) {
      if (data) {
        return res.send (
          responseMiddleWares (
            'password changed successfully',
            true,
            undefined,
            200
          )
        );
      } else {
        return res.send (
          responseMiddleWares (
            "didn't find user of this mobileno register first",
            false,
            undefined,
            400
          )
        );
      }
    });
  } catch (err) {
    return res.send (
      responseMiddleWares (
        'something went wrong please contact admin',
        false,
        err,
        400
      )
    );
  }
};
