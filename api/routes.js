const express = require("express");
let route = require("express").Router();
const app = express();

const addListing = require("./routes/vendor/addListing");
app.use("/vendor/add", addListing)

const vendorRoute = require("./routes/vendor/authVendor");
app.use("/vendor", vendorRoute);

const categoryRoute = require("./routes/vendor/category");
app.use("/vendor",categoryRoute)

const adminCatergory = require("./routes/admin/category");
app.use("/admin/category", adminCatergory);


const adminVendorMangager = require("./routes/admin/vendorManager")
app.use("/admin/vendor_manager", adminVendorMangager)

const adminRoute = require("./routes/admin/verifyVendor");
app.use("/admin", adminRoute);

module.exports = app  
