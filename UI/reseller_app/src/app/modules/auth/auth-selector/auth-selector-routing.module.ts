import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthSelectorPage } from './auth-selector.page';

const routes: Routes = [
  {
    path: '',
    component: AuthSelectorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthSelectorPageRoutingModule {}
