import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthSelectorPageRoutingModule } from './auth-selector-routing.module';

import { AuthSelectorPage } from './auth-selector.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuthSelectorPageRoutingModule
  ],
  declarations: [AuthSelectorPage]
})
export class AuthSelectorPageModule {}
