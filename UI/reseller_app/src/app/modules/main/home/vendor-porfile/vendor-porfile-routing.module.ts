import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VendorPorfilePage } from './vendor-porfile.page';

const routes: Routes = [
  {
    path: '',
    component: VendorPorfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendorPorfilePageRoutingModule {}
