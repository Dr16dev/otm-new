import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VendorPorfilePageRoutingModule } from './vendor-porfile-routing.module';

import { VendorPorfilePage } from './vendor-porfile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VendorPorfilePageRoutingModule
  ],
  declarations: [VendorPorfilePage]
})
export class VendorPorfilePageModule {}
