import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OurPicksPage } from './our-picks.page';

const routes: Routes = [
  {
    path: '',
    component: OurPicksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OurPicksPageRoutingModule {}
