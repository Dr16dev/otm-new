import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OurPicksPageRoutingModule } from './our-picks-routing.module';

import { OurPicksPage } from './our-picks.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OurPicksPageRoutingModule
  ],
  declarations: [OurPicksPage]
})
export class OurPicksPageModule {}
